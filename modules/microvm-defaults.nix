# No MicroVM settings but some defaults that enable evaulating NixOS
# configurations that are destined to be used on Skyflake

{ config, lib, ... }:

{
  # autoupdates do not make sense inside MicroVMs with read-only /nix/store
  c3d2.autoUpdate = false;

  boot = {
    loader.grub.enable = false;
    kernel.sysctl =
      let
        mem = if (config?microvm) then config.microvm.mem else config.deployment.mem;
      in
      lib.optionalAttrs (mem <= 2*1024) {
        # table overflow causing packets from nginx to the service to drop
        # nf_conntrack: nf_conntrack: table full, dropping packet
        "net.netfilter.nf_conntrack_max" = "65536";
      };
    kernelModules = [
      # required for net.netfilter.nf_conntrack_max appearing in sysfs early at boot
      "nf_conntrack"
    ];
    kernelParams = [
      "preempt=none"
      # No server/router runs any untrusted user code
      "mitigations=off"
    ];
  };

  fileSystems."/" = lib.mkDefault {
    fsType = "tmpfs";
  };

  hardware.enableRedistributableFirmware = false;

  # nix store is mounted read only
  nix = {
    enable = false;
    gc.automatic = false;
  };

  systemd.tmpfiles.rules = [
    "d /home/root 0700 root root -" # createHome does not create it
  ];

  users = {
    mutableUsers = false;
    # store root users files persistent, especially .bash_history
    users."root" = {
      createHome = true;
      home = lib.mkForce "/home/root";
    };
  };
}
