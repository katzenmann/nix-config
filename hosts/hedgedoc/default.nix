{ config, pkgs, ... }:

{
  c3d2.deployment.server = "server10";

  microvm.mem = 1536;

  networking.hostName = "hedgedoc";

  services = {
    backup = {
      enable = true;
      paths = [ "/var/lib/hedgedoc/" ];
    };

    hedgedoc = {
      enable = true;
      ldap.enable = true;
      settings = {
        allowAnonymousEdits = true;
        allowFreeURL = true;
        allowOrigin = [ "hedgedoc.c3d2.de" ];
        csp = {
          enable = true;
          addDefaults = true;
          upgradeInsecureRequest = "auto";
        };
        db = {
          dialect = "postgres";
          host = "/run/postgresql/";
        };
        defaultPermission = "freely";
        domain = "hedgedoc.c3d2.de";
        loglevel = "warn";
        path = "/run/hedgedoc/hedgedoc.sock";
        protocolUseSSL = true;
        sessionSecret = "$sessionSecret";
      };
      environmentFile = config.sops.secrets."hedgedoc".path;
    };

    nginx = {
      enable = true;
      enableReload = true;
      upstreams.hedgedoc.servers."unix:${config.services.hedgedoc.settings.path}" = { };
      virtualHosts = {
        "codimd.c3d2.de" = {
          forceSSL = true;
          enableACME = true;
          locations."/".return = "301 https://hedgedoc.c3d2.de$request_uri";
        };
        "hackmd.c3d2.de" = {
          forceSSL = true;
          enableACME = true;
          locations."/".return = "301 https://hedgedoc.c3d2.de$request_uri";
        };
        "hedgedoc.c3d2.de" = {
          default = true;
          forceSSL = true;
          enableACME = true;
          locations = {
            "^~ /robots.txt".return = "200 'User-agent: *\\nDisallow: /'";
            "/".proxyPass = "http://hedgedoc";
          };
        };
      };
    };

    portunus.addToHosts = true;

    postgresql = {
      enable = true;
      ensureDatabases = [
        "hedgedoc"
      ];
      ensureUsers = [ {
        name = "hedgedoc";
        ensurePermissions = {
          "DATABASE \"hedgedoc\"" = "ALL PRIVILEGES";
        };
      }];
      package = pkgs.postgresql_16;
      upgrade.stopServices = [ "hedgedoc" ];
    };
  };

  sops = {
    age.sshKeyPaths = [ "/etc/ssh/ssh_host_ed25519_key" ];
    defaultSopsFile = ./secrets.yaml;
    secrets = {
      "hedgedoc".owner = config.systemd.services.hedgedoc.serviceConfig.User;
    };
  };

  systemd = {
    services.hedgedoc = {
      preStart = ''
        rm -f ${config.services.hedgedoc.settings.path}
      '';
      serviceConfig.UMask = "0007";
    };
    tmpfiles.rules = [
      "d /run/hedgedoc/ 0770 hedgedoc hedgedoc -"
    ];
  };

  system.stateVersion = "22.11";

  users.users.nginx.extraGroups = [ "hedgedoc" ];
}
