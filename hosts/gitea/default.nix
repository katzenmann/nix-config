{ config, pkgs, lib, libC, ... }:

{
  c3d2.deployment.server = "server10";

  microvm.mem = 4 * 1024;

  networking = {
    hostName = "gitea";
    firewall.allowedTCPPorts = [ 2222 ];
  };

  services = {
    backup = {
      enable = true;
      paths = [ "/var/lib/gitea/" ];
      exclude = [
        "/var/lib/gitea/data/indexers/"
        "/var/lib/gitea/data/repo-archive"
        "/var/lib/gitea/data/queues"
        "/var/lib/gitea/data/tmp/"
      ];
    };

    gitea = {
      enable = true;
      appName = "Gitea: with a cup of Kolle Mate";
      database.type = "postgres";
      lfs.enable = true;
      repositoryRoot = "/var/lib/gitea/repositories";

      dump = {
        # Is a nice feature once we have a dedicated backup storage.
        # For now it is disabled, since it delays `nixos-rebuild switch`.
        enable = false;
        backupDir = "/var/backup/gitea/";
      };

      ldap = {
        enable = true;
        bindPasswordFile = config.sops.secrets."gitea/ldapSearchUserPassword".path;
      };

      settings = {
        # we use drone for internal tasks and don't want people to execute code on our infrastructure
        actions.ENABLED = false;
        "cron.delete_generated_repository_avatars".ENABLED = true;
        "cron.delete_old_system_notices".ENABLED = true;
        "cron.repo_health_check".TIMEOUT = "300s";
        "cron.resync_all_sshkeys" = {
          ENABLED = true;
          RUN_AT_START = true;
        };
        database.LOG_SQL = false;
        # enable if it is actually useful
        # federation.ENABLED = true;
        indexer.REPO_INDEXER_ENABLED = true;
        log = {
          LEVEL = "Info";
          "logger.router.MODE" = "Warn";
          "logger.xorm.MODE" = "Warn";
        };
        mailer = {
          ENABLED = true;
          FROM = "gitea@c3d2.de";
          PROTOCOL = "sendmail";
          SENDMAIL_PATH = "/run/wrappers/bin/sendmail";
          SENDMAIL_ARGS = "--";
        };
        other.SHOW_FOOTER_VERSION = false;
        # disabled to prevent us becoming critical infrastructure, might revisit later
        packages.ENABLED = false;
        picture = {
          # this also disables libravatar
          DISABLE_GRAVATAR = false;
          ENABLE_FEDERATED_AVATAR = true;
          GRAVATAR_SOURCE = "libravatar";
          REPOSITORY_AVATAR_FALLBACK = "random";
        };
        repository.DEFAULT_REPO_UNITS = "repo.code,repo.releases,repo.issues,repo.pulls";
        server = rec {
          DOMAIN = "gitea.c3d2.de";
          ENABLE_GZIP = true;
          SSH_AUTHORIZED_KEYS_BACKUP = false;
          SSH_DOMAIN = DOMAIN;
        };
        service = {
          DISABLE_REGISTRATION = true;
          ENABLE_NOTIFY_MAIL = true;
          NO_REPLY_ADDRESS = "no_reply@c3d2.de";
          REGISTER_EMAIL_CONFIRM = true;
          USER_LOCATION_MAP_URL = "https://www.openstreetmap.org/search?query=";
        };
        session = {
          COOKIE_SECURE = lib.mkForce true;
          PROVIDER = "db";
          SAME_SITE = "strict";
        };
        "ssh.minimum_key_sizes" = {
          ECDSA = -1;
          RSA = 4095;
        };
        time.DEFAULT_UI_LOCATION = config.time.timeZone;
        ui = {
          DEFAULT_THEME = "arc-green";
          EXPLORE_PAGING_NUM = 25;
          FEED_PAGING_NUM = 50;
          ISSUE_PAGING_NUM = 25;
        };
      };
    };

    nginx = {
      enable = true;
      virtualHosts."gitea.c3d2.de" = {
        forceSSL = true;
        enableACME = true;
        listen = libC.defaultListen;
        locations."/".proxyPass = "http://localhost:3000";
      };
    };

    openssh = {
      enable = true;
      extraConfig = ''
        Match User gitea
          AllowAgentForwarding no
          AllowTcpForwarding no
          PermitTTY no
          X11Forwarding no
      '';
    };

    portunus.addToHosts = true;

    postgresql = {
      package = pkgs.postgresql_16;
      upgrade.stopServices = [ "gitea" ];
    };
  };

  sops = {
    defaultSopsFile = ./secrets.yaml;
    secrets = {
      "gitea/ldapSearchUserPassword".owner = "gitea";
    };
  };

  programs.msmtp = {
    enable = true;
    accounts.default = {
      host = "mail.c3d2.de";
      port = 587;
      tls = true;
      tls_starttls = true;
      auth = false;
      domain = "gitea.c3d2.de";
      from = "mail@c3d2.de";
    };
  };

  system.stateVersion = "21.11";
}
