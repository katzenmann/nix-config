{ config, lib, pkgs, ... }:

{
  imports = [
    ./hardware-configuration.nix
    ./microvm-staging.nix
  ];

  c3d2 = {
    baremetal = true;
    deployment.microvmBaseZfsDataset = "server10/vm";
    hq.statistics.enable = true;
  };

  boot = {
    loader.grub = {
      enable = true;
      device = "/dev/sda";
    };
    kernelParams = [
      "preempt=none"
      # No server/router runs any untrusted user code
      "mitigations=off"
    ];
    tmp = {
      useTmpfs = true;
      tmpfsSize = "80%";
    };
  };

  networking = {
    hostName = "server10";
    # TODO: change that to something more random
    hostId = "10101010";
  };

  # services = {
  #   ceph = {
  #     mds.package = pkgs.ceph_17_2;
  #     mgr.package = pkgs.ceph_17_2;
  #     mon.package = pkgs.ceph_17_2;
  #     osd.package = pkgs.ceph_17_2;
  #     rgw.package = pkgs.ceph_17_2;
  #   };

  #   # reserve resources for legacy MicroVMs
  #   nomad.settings.client.reserved = {
  #     cpu = 4200;
  #     # see /sys/fs/cgroup/system.slice/system-microvm.slice/memory.current
  #     memory = 28 * 1024;
  #   };
  # };

  simd.arch = "ivybridge";

  sops = {
    defaultSopsFile = ./secrets.yaml;
    secrets."machine-id" = {
      mode = "444";
      path = "/etc/machine-id";
    };
    # secrets."ceph/osd.4/keyfile" = { };
  };

  # static list of microvms from other sources
  microvm.autostart = [
    "data-hoarder"
    "staging-data-hoarder"
    "borken-data-hoarder"
    "tram-borzoi"
    "uranus"
    "dresden-zone"
  ];

  # skyflake = {
  #   nomad.client.meta."c3d2.cpuSpeed" = "4";
  #   storage.ceph.osds = [{
  #     id = 4;
  #     fsid = "21ff9a57-c8d1-4cfa-8e01-c09ae0c2f0e3";
  #     path = "/dev/zvol/server10/ceph-osd.4";
  #     keyfile = config.sops.secrets."ceph/osd.4/keyfile".path;
  #     deviceClass = "ssd";
  #   }];
  #   # TODO: remove
  #   storage.ceph.package = lib.mkForce pkgs.ceph_17_2;
  # };

  system.stateVersion = "21.11"; # Did you read the comment?
}
