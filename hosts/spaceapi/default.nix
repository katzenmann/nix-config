_:

{
  c3d2.deployment.server = "server10";

  networking = {
    firewall = {
      allowedTCPPorts = [
        3000 # spaceapi
      ];
      allowedUDPPorts = [
        25826 # spaceapi <- collectd
      ];
    };
    hostName = "spaceapi";
  };

  services.spaceapi.enable = true;

  system.stateVersion = "19.03";
}
