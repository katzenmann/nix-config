{ config, lib, pkgs, ... }:

{
  imports = [
    ./hardware-configuration.nix
  ];

  c3d2 = {
    baremetal = true;
    # deployment.microvmBaseZfsDataset = "tank/storage";
    hq.statistics.enable = true;
  };

  boot = {
    loader.grub = {
      enable = true;
      # Define on which hard drive you want to install Grub.
      device = "/dev/disk/by-id/scsi-3600300570140a6102b0acad9825149f2"; # or "nodev" for efi only
    };
    kernelParams = [
      "preempt=none"
      # No server/router runs any untrusted user code
      "mitigations=off"
    ];
    tmp = {
      useTmpfs = true;
      tmpfsSize = "80%";
    };
  };

  networking = {
    hostName = "server8";
    hostId = "08080808";
  };

  # services.ceph = {
  #   mds.package = pkgs.ceph_17_2;
  #   mgr.package = pkgs.ceph_17_2;
  #   mon.package = pkgs.ceph_17_2;
  #   osd.package = pkgs.ceph_17_2;
  #   rgw.package = pkgs.ceph_17_2;
  # };

  simd.arch = "westmere";

  sops = {
    defaultSopsFile = ./secrets.yaml;
    secrets = {
      # "ceph/osd.1/keyfile" = { };
      # "ceph/osd.2/keyfile" = { };
      "machine-id" = {
        mode = "444";
        path = "/etc/machine-id";
      };
    };
  };

  # skyflake = {
  #   nomad.client.meta."c3d2.cpuSpeed" = "3";
  #   storage.ceph.osds = [{
  #     id = 1;
  #     fsid = "4b196252-efb6-4ad2-9e9b-cc3fcd664a3a";
  #     path = "/dev/zvol/server8_root/ceph-osd.1";
  #     keyfile = config.sops.secrets."ceph/osd.1/keyfile".path;
  #     deviceClass = "ssd";
  #   }
  #     {
  #       id = 2;
  #       fsid = "b860ec59-3314-4fd1-be45-35a46fd8c059";
  #       path = "/dev/zvol/server8_hdd/ceph-osd.2";
  #       keyfile = config.sops.secrets."ceph/osd.2/keyfile".path;
  #       deviceClass = "hdd";
  #     }];
  #   # TODO: remove
  #   storage.ceph.package = lib.mkForce pkgs.ceph_17_2;
  # };

  system.stateVersion = "22.11";
}
