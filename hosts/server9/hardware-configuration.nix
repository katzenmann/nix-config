# Do not modify this file!  It was generated by ‘nixos-generate-config’
# and may be overwritten by future invocations.  Please make changes
# to /etc/nixos/configuration.nix instead.
{ config, lib, modulesPath, ... }:

{
  imports =
    [ (modulesPath + "/installer/scan/not-detected.nix")
    ];

  boot.initrd.availableKernelModules = [ "uhci_hcd" "ehci_pci" "ahci" "megaraid_sas" "usb_storage" "usbhid" "sd_mod" "sr_mod" ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ "kvm-intel" ];
  boot.extraModulePackages = [ ];
  boot.supportedFilesystems = [ "zfs" ];

  fileSystems."/" =
    { device = "/dev/disk/by-uuid/58fdc536-9949-4921-bd9c-c05cdc8fb9ad";
      fsType = "ext4";
    };

  fileSystems."/boot" =
    { device = "/dev/disk/by-uuid/E4C9-03A7";
      fsType = "vfat";
    };

  fileSystems."/tank" =
    { device = "tank";
      fsType = "zfs";
      options = [ "zfsutil" ];
    };

  fileSystems."/tank/poelzi" =
    { device = "tank/poelzi";
      fsType = "zfs";
      options = [ "zfsutil" ];
    };

  fileSystems."/tank/storage" =
    { device = "tank/storage";
      fsType = "zfs";
      options = [ "zfsutil" ];
    };

  fileSystems."/tank/backup_vms_ceph_migration" =
    { device = "tank/backup_vms_ceph_migration";
      fsType = "zfs";
      options = [ "zfsutil" ];
    };

  fileSystems."/tank/dump-dvb-iq-storage" =
    { device = "tank/dump-dvb-iq-storage";
      fsType = "zfs";
      options = [ "zfsutil" ];
    };

  fileSystems."/tank/storage/ftp" =
    { device = "tank/storage/ftp";
      fsType = "zfs";
      options = [ "zfsutil" ];
    };

  fileSystems."/tank/storage/stream" =
    { device = "tank/storage/stream";
      fsType = "zfs";
      options = [ "zfsutil" ];
    };

  swapDevices = [ ];

  hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
}
