{ config, lib, pkgs, ... }:

{
  imports = [
    ./hardware-configuration.nix
  ];

  c3d2 = {
    baremetal = true;
    deployment.microvmBaseZfsDataset = "tank/storage";
    hq.statistics.enable = true;
  };

  boot = {
    loader.grub = {
      enable = true;
      device = "/dev/sdc";
    };
    kernelParams = [
      "preempt=none"
      # No server/router runs any untrusted user code
      "mitigations=off"
    ];
    tmp = {
      useTmpfs = true;
      tmpfsSize = "80%";
    };
  };

  networking = {
    hostName = "server9";
    hostId = "09090909";
  };

  # required by libvirtd
  security.polkit.enable = true;

  services = {
    # ceph = {
    #   mds.package = pkgs.ceph_17_2;
    #   mgr.package = pkgs.ceph_17_2;
    #   mon.package = pkgs.ceph_17_2;
    #   osd.package = pkgs.ceph_17_2;
    #   rgw.package = pkgs.ceph_17_2;
    # };

    nginx = {
      enable = true;
      additionalModules = [ pkgs.nginxModules.fancyindex ];
      virtualHosts."server9.cluster.zentralwerk.org" = {
        default = true;
        forceSSL = true;
        enableACME = true;
        locations = {
          "/archive".return = "307 /archive/";
          "/archive/" = {
            alias = "/tank/owncloud-archive/";
            extraConfig = ''
              fancyindex on;
              fancyindex_exact_size off;
            '';
          };
          "/restic/" = {
            proxyPass = "http://${config.services.restic.server.listenAddress}/";
            extraConfig = ''
              client_max_body_size 40M;
              proxy_buffering off;
            '';
          };
        };
      };
    };

    # reserve resources for libvirt VMs
    # nomad.settings.client.reserved = {
    #   cpu = 2300;
    #   memory = 16 * 1024;
    # };

    restic.server = {
      enable = true;
      listenAddress = "127.0.0.1:8080";
      privateRepos = true;
    };
  };

  simd.arch = "westmere";

  sops = {
    defaultSopsFile = ./secrets.yaml;
    secrets = {
      "machine-id" = {
        mode = "444";
        path = "/etc/machine-id";
      };
      # "ceph/osd.3/keyfile" = { };
      # "ceph/osd.7/keyfile" = { };
      "restic/htpasswd" = {
        owner = config.systemd.services.restic-rest-server.serviceConfig.User;
        path = "/var/lib/restic/.htpasswd";
      };
    };
  };

  # skyflake = {
  #   nomad.client.meta."c3d2.cpuSpeed" = "3";
  #   storage.ceph.osds = [{
  #     id = 3;
  #     fsid = "54d56ab8-fc43-4e16-886d-3c82dcc1f8fe";
  #     path = "/dev/zvol/tank/ceph-osd.3";
  #     keyfile = config.sops.secrets."ceph/osd.3/keyfile".path;
  #     deviceClass = "hdd";
  #   }
  #     {
  #       id = 7;
  #       fsid = "a5450c3b-2e20-450b-a17a-d7938ee9d262";
  #       path = "/dev/disk/by-id/wwn-0x600300570140a0c02c39f0863bd3c53e";
  #       keyfile = config.sops.secrets."ceph/osd.7/keyfile".path;
  #       deviceClass = "ssd";
  #     }];
  #   # TODO: remove
  #   storage.ceph.package = lib.mkForce pkgs.ceph_17_2;
  # };

  system.stateVersion = "21.11";

  # TODO: enable for zw-ev and poelzi-ha until we find a better solution
  virtualisation.libvirtd = {
    enable = true;
    onShutdown = "shutdown";
  };

}
