{ config, ... }:
{
  c3d2 = {
    deployment.server = "server10";
    hq.statistics.enable = true;
  };

  microvm = {
    mem = 512;
    vcpu = 8;
  };

  networking.hostName = "activity-relay";
  services.journald.extraConfig = ''
    Storage=volatile
  '';

  services = {
    activity-relay = {
      enable = true;
      jobConcurrency = config.microvm.vcpu;
      relay = {
        bind = "127.0.0.1:8080";
        domain = "activity-relay.serv.zentralwerk.org";
      };
    };

    backup = {
      enable = true;
      paths = [ "/var/lib/activity-relay/" ];
    };

    redis.enable = true;

    nginx = {
      enable = true;
      virtualHosts."activity-relay.serv.zentralwerk.org" = {
        forceSSL = true;
        enableACME = true;
        locations."/".proxyPass = "http://${config.services.activity-relay.relay.bind}/";
      };
    };
  };

  sops.defaultSopsFile = ./secrets.yaml;

  system.stateVersion = "23.05";
}
