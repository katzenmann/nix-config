{ config, lib, libC, pkgs, ... }:

{
  c3d2.deployment.server = "server10";

  microvm.mem = 2 * 1024;

  networking.hostName = "matemat";

  services = {
    backup.enable = true;

    nginx = {
      enable = true;
      virtualHosts."matemat.hq.c3d2.de" = {
        default = true;
        forceSSL = true;
        enableACME = true;
        locations."/" = {
          proxyPass = "http://localhost:3000";
          extraConfig = libC.hqNetworkOnly + ''
            auth_basic secured;
            auth_basic_user_file ${config.sops.secrets."nginx/basic-auth".path};
          '';
        };
      };
    };

    postgresql = {
      package = pkgs.postgresql_16;
      upgrade.stopServices = [ "yammat" ];
    };

    yammat = {
      enable = true;
      config = lib.mkOptionDefault ''
        copyright_link: "https://gitea.c3d2.de/c3d2/yammat"
      '';
    };
  };

  programs.msmtp = {
    enable = true;
    accounts.default = {
      host = "mail.c3d2.de";
      port = 587;
      tls = true;
      tls_starttls = true;
      auth = false;
      domain = "matemat.hq.c3d2.de";
      from = "nek0@c3d2.de";
    };
  };

  sops = {
    defaultSopsFile = ./secrets.yaml;
    secrets = {
      "nginx/basic-auth".owner = "nginx";
    };
  };

  system.stateVersion = "22.05";
}
