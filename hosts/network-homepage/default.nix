{ zentralwerk, pkgs, ... }:

{
  c3d2 = {
    hq.statistics.enable = true;
    deployment.server = "server10";
  };

  networking.hostName = "network-homepage";

  services = {
    nginx = rec {
      enable = true;
      virtualHosts."www.zentralwerk.org" = {
        forceSSL = true;
        enableACME = true;
        root = "${zentralwerk.packages.${pkgs.system}.homepage}/share/doc/zentralwerk/www";
      };
      virtualHosts."zentralwerk.org" = virtualHosts."www.zentralwerk.org" // {
        default = true;
      };
    };
  };

  system.stateVersion = "22.05";
}
