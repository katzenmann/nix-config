_:

{
  c3d2 = {
    deployment.server = "server10";
    hq.statistics.enable = true;
  };

  # deployment.mem = 1024;

  networking.hostName = "gnunet";

  services.gnunet = {
    enable = true;
    load = {
      # bits/s
      maxNetDownBandwidth = 1000 * 1000 * 1000;
      maxNetUpBandwidth = 1 * 1000 * 1000;
    };
  };

  system.stateVersion = "22.05";
}
