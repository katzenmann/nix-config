_:

{
  defaultListen = let
    listen = [
      {
        addr = "[::]";
        port = 80;
      }
      {
        addr = "[::]";
        port = 443;
        ssl = true;
      }
      {
        addr = "[::]";
        port = 8080;
        extraParameters = [ "proxy_protocol" ];
      }
      {
        addr = "[::]";
        port = 8443;
        ssl = true;
        extraParameters = [ "proxy_protocol" ];
      }
    ];
  in
  map (x: (x // { addr = "0.0.0.0"; })) listen ++ listen;

  hqNetworkOnly = ''
    satisfy any;
    allow 2a00:8180:2c00:200::/56;
    allow 2a0f:5382:acab:1400::/56;
    allow fd23:42:c3d2:500::/56;
    allow 30c:c3d2:b946:76d0::/64;
    allow ::1/128;
    allow 172.22.99.0/24;
    allow 172.20.72.0/21;
    allow 127.0.0.0/8;
    deny all;
  '';
}
