{  bevy-julia
, bevy-mandelbrot
, tracer
}:

final: prev:

with final; {
  activity-relay = callPackage ./activity-relay.nix { };

  allcolors = callPackage ./allcolors.nix { };

  inherit (bevy-julia.packages.${system}) bevy_julia;
  inherit (bevy-mandelbrot.packages.${system}) bevy_mandelbrot;

  bmxd = callPackage ./bmxd.nix { };

  ceph_17_2 = assert (lib.versions.majorMinor ceph.version) == "17.2"; prev.ceph;

  dex-oidc = prev.dex-oidc.override {
    buildGoModule = let
      ver = lib.versions.majorMinor prev.dex-oidc.version;
    in args: buildGoModule (args // {
      patches = args.patches or [ ]
        # remember session
        # TODO: remove 2.35 when 23.05 is stable
        ++ lib.optional (ver == "2.35") (fetchpatch {
          url = "https://github.com/dexidp/dex/commit/dd0fb05386ce89c74381ce49e903cc10b987459e.patch";
          hash = "sha256-71py0pysgS3jDkKeqD/K4KJ821bolz/4PTjt2rDdUy8=";
        })
        ++ lib.optional (ver == "2.36") (fetchpatch {
          url = "https://github.com/dexidp/dex/commit/000004b13b876e04a6f75ec0394f7cabe84fb15e.patch";
          hash = "sha256-u85RnwfhcQt7RK11Ed/fDLUbHOuD+TKJU8UHQslZowM=";
        });
    } // lib.optionalAttrs (ver == "2.35") {
      vendorSha256 = "sha256-BxFiRHOGIJf3jTVtrw/QbnvG5gyfwAKQGd3IiWw5iVc=";
    } // lib.optionalAttrs (ver == "2.36") {
      vendorHash = "sha256-hxq7JPz8uD5WQIPO2anSf9+kzyoQy/BQ0OVTblA8qts=";
    });
  };

  dump1090-influxdb = callPackage ./dump1090-influxdb { };

  dump1090_rs = callPackage ./dump1090_rs.nix { };

  chromium = prev.chromium.override {
    # enable hardware accerlation with vaapi, force dark mode, detect kwallet
    commandLineArgs = "--enable-features=VaapiVideoEncoder,VaapiVideoDecoder,CanvasOopRasterization --force-dark-mode --password-store=detect";
  };

  firefox = prev.firefox.override {
    extraPolicies = {
      DisablePocket = true;
      FirefoxHome.Pocket = false;
    };
  };

  # throw illegal instructions in tests
  # TODO: check this on 23.11
  gdal = prev.gdal.override { useTiledb = false; };

  grafana = prev.grafana.overrideAttrs (_: {
    # broken with backports and take a good amount of time
    doCheck = false;
  });

  # hydra flake
  hydra = prev.hydra.overrideAttrs (oldAttrs: {
    patches = oldAttrs.patches or [ ] ++ [
      # gitea webhook support
      (fetchpatch {
        url = "https://github.com/NixOS/hydra/pull/1227/commits/750978a19232583e17620a1bd80435e957e7213a.patch";
        sha256 = "sha256-86Li0YUSVUdnw6lt6kZ56ohDRKPD13SZzukqPU1np8U=";
      })
      # fix github webhook from orgs
      (fetchpatch {
        url = "https://github.com/NixOS/hydra/commit/4d664ecb0faaf51b21673f979b543ea4694c3f1b.patch";
        sha256 = "sha256-lF5Rnz8r9ptyMLhcg/XnjiNhOK1KcLA7hi01ye4KgmI=";
      })
    ];
  });

  # hydra in nixpkgs
  hydra_unstable = prev.hydra_unstable.overrideAttrs (oldAttrs: {
    patches = oldAttrs.patches or [ ] ++ [
      # gitea webhook support
      (fetchpatch {
        url = "https://github.com/NixOS/hydra/pull/1227/commits/750978a19232583e17620a1bd80435e957e7213a.patch";
        sha256 = "sha256-86Li0YUSVUdnw6lt6kZ56ohDRKPD13SZzukqPU1np8U=";
      })
      # fix github webhook from orgs
      (fetchpatch {
        url = "https://github.com/NixOS/hydra/commit/4d664ecb0faaf51b21673f979b543ea4694c3f1b.patch";
        sha256 = "sha256-lF5Rnz8r9ptyMLhcg/XnjiNhOK1KcLA7hi01ye4KgmI=";
      })
    ];
  });

  mlat-client = python3Packages.callPackage ./mlat-client.nix { };

  nixVersions = prev.nixVersions // {
    stable = (prev.nixVersions.stable.override { withAWS = false; }).overrideAttrs ({ patches ? [ ], ...}: {
      patches = patches ++ [
        # request compression
        (fetchpatch {
          url = "https://github.com/NixOS/nix/pull/7712.patch";
          sha256 = "sha256-mAx2h0/r7HayvTjMMxmewaD+L4OOB2gRJaQb3JEb0rk=";
        })
      ];
    });
  };

  openssh = prev.openssh.overrideAttrs (_: {
    # takes 30 minutes
    doCheck = false;
  });

  pi-sensors = callPackage ./pi-sensors { };

  plume = callPackage ./plume { };

  readsb = callPackage ./readsb.nix { };

  schalterd = callPackage ./schalterd.nix { };

  telme10 = callPackage ./telme10.nix { };

  tracer-game =
    if true
    then throw "tracer-game: haddock runs on affection for 10 hours and more"
    else tracer.packages.${system}.tracer-game;

  trainbot = callPackage ./trainbot.nix { };
}
