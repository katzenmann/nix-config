{ buildGoModule
, fetchFromGitHub
}:

buildGoModule {
  pname = "telme10";
  version = "unstable";

  src = fetchFromGitHub {
    owner = "c3d2";
    repo = "telme10";
    rev = "5962266909bada6993a500ecce8707ec486d4cc0";
    sha256 = "sha256-i6jKT2on8s9z5bLhCC4nYoLbngcbfTJykMW9JGc0/dY=";
  };

  postPatch = ''
    substituteInPlace telme10.service \
      --replace "/usr/local/bin/telme10" "${placeholder "out"}/bin/telme10 [::]:23"
  '';

  vendorSha256 = "sha256-SzYAXvWE2qt7aPX99AhgTQe7tmGuaBuOUZNNg7+CvCQ=";

  postInstall = ''
    install -Dt $out/etc/systemd/system telme10.service
    install -Dt $out/etc/systemd/system telme10.socket
  '';
}
