{ lib
, stdenv
, buildGoModule
, fetchFromGitHub
, ffmpeg
}:

buildGoModule {
  pname = "trainbot";
  version = "unstable-2023-05-25";

  src = fetchFromGitHub {
    owner = "jo-m";
    repo = "trainbot";
    rev = "3a03711c99ff157a793dddc20a59116eb7cd1664";
    sha256 = "sha256-JdilVe/jysTVBg2Q/IrLIzODVz+PG+1HGo+5AF+X6D4=";
  };

  checkInputs = [ ffmpeg ];
  doCheck = false;

  vendorHash = "sha256-IsYUvVmZdlwEaOoD76m9KABsldBado9yQiOa8Q8Pkp0=";
}
