{ pkgsCross, fetchFromGitHub }:

pkgsCross.armv7l-hf-multiplatform.pkgsStatic.rustPlatform.buildRustPackage {
  name = "schalterd";

  src = "${fetchFromGitHub {
    owner = "astro";
    repo = "spacemsg";
    # master of 2023-07-02
    rev = "a825a738544e62c285f4497c151a73d417326da2";
    sha256 = "sha256-8sM2GdQ2nJ3YCCF5+ZW0vBNTKL3/ulY1/fmyw++5UQQ=";
  }}/schalterd";

  cargoSha256 = "sha256-OdNztl4XQML2UqK/4BLzKed3pBJNd9rIwHEXaIzLQ4U=";
}
