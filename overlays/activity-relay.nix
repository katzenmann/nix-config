{ lib, buildGoModule, fetchFromGitHub
}:

buildGoModule rec {
  pname = "activity-relay";
  version = "2.0.4";

  src = fetchFromGitHub {
    owner = "yukimochi";
    repo = pname;
    rev = "v${version}";
    hash = "sha256-0mZrkOrlg4AZFD8K6bcLPx7Xty7mRqjlxStwxXna70c=";
  };

  vendorHash = "sha256-EHfRDFzfoi9YiKoeRasWSiyqT1HrIK8gIPadpyNYgbU=";

  # tests require internet access
  doCheck = false;

  meta = with lib; {
    mainProgram = "Activity-Relay";
    description = "Yet another powerful customizable ActivityPub relay server written in Go.";
    homepage = "https://github.com/yukimochi/Activity-Relay";
    license = licenses.agpl3;
    maintainers = with maintainers; [ astro ];
  };
}
